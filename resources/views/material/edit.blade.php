@extends('layouts.app')

@section('content')
    <div class="page-header mb-5">
        <h1>Edit material - {{ $material->name }}</h1>
    </div>

    @include('material.forms.material')
@endsection