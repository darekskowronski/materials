@extends('layouts.app')

@section('content')
    <div class="page-header mb-5">
        <h1>Create new material</h1>
    </div>

    @include('material.forms.material')
@endsection