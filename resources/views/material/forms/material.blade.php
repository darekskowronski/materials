<form method="post" action="{{ isset($material) ? route('materials.update', ['id' => $material->getKey()]) : route('materials.store') }}">
    {{ csrf_field() }}
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : null }}" id="name" name="name" value="{{ isset($material) ? $material->name : null }}" required>
                {!! $errors->has('name') ? '<div class="invalid-feedback">' . $errors->first('name') . '</div>' : null !!}
            </div>
        </div>
        
        <div class="col-sm-6">
            <div class="form-group">
                <label for="code">Code</label>
                <input type="code" class="form-control {{ $errors->has('code') ? 'is-invalid' : null }}" id="code" name="code" value="{{ isset($material) ? $material->code : null }}" required>
                {!! $errors->has('code') ? '<div class="invalid-feedback">' . $errors->first('code') . '</div>' : null !!}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="group_id">Group</label>
                <select class="form-control {{ $errors->has('group_id') ? 'is-invalid' : null }}" name="group_id" id="group_id" required>
                    <option value="">(select)</option>
                    @foreach($groups as $item)
                        <option value="{{ $item->getKey() }}" {{ isset($material) && $material->group_id == $item->getKey() ? 'selected' : null }}>{{ $item->name }}</option>
                    @endforeach
                </select>
                {!! $errors->has('group_id') ? '<div class="invalid-feedback">' . $errors->first('group_id') . '</div>' : null !!}
            </div>
        </div>
        
        <div class="col-sm-6">
            <div class="form-group">
                <label for="unit_id">Unit</label>
                <select class="form-control {{ $errors->has('unit_id') ? 'is-invalid' : null }}" name="unit_id" id="unit_id">
                    <option value="">(select)</option>
                    @foreach($units as $item)
                        <option value="{{ $item->getKey() }}" {{ isset($material) && $material->unit_id == $item->getKey() ? 'selected' : null }}>{{ $item->name }}</option>
                    @endforeach
                </select>
                {!! $errors->has('unit_id') ? '<div class="invalid-feedback">' . $errors->first('unit_id') . '</div>' : null !!}
            </div>
        </div>
    </div>
    
    @if(isset($material)) 
        <button type="submit" name="_method" value="put" class="btn btn-primary">Update</button>
    @else
        <button type="submit" class="btn btn-success">Add</button>
    @endif
</form>