@extends('layouts.app')

@section('content')
    <div class="page-header mb-5">
        <h1>Materials</h1>
        <a href="{{ route('materials.create') }}" class="btn btn-primary">Add new material</a>
    </div>

    <ul>
        @foreach($materials as $item)
            <li>
                <a href="{{ route('materials.edit', ['id' => $item->getKey()]) }}">{{ $item->name }}</a>
            </li>
        @endforeach
    </ul>
@endsection