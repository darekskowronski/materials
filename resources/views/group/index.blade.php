@extends('layouts.app')

@section('content')
    <div class="page-header mb-5">
        <h1>Groups</h1>
        <a href="{{ route('groups.create') }}" class="btn btn-primary">Add new group</a>
    </div>

    <ul>
        @foreach($groups as $item)
            <li>
                <a href="{{ route('groups.edit', ['id' => $item->getKey()]) }}">
                    {{ $item->name }}
                    {{ isset($item->parentGroup) ? '(parent: ' . $item->parentGroup->name . ')' : null }}
                </a>
            </li>
        @endforeach
    </ul>
@endsection