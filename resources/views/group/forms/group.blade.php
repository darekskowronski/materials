<form method="post" action="{{ isset($group) ? route('groups.update', ['id' => $group->getKey()]) : route('groups.store') }}">
    {{ csrf_field() }}
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : null }}" id="name" name="name" value="{{ isset($group) ? $group->name : null }}" required>
                {!! $errors->has('name') ? '<div class="invalid-feedback">' . $errors->first('name') . '</div>' : null !!}
            </div>
        </div>
        
        <div class="col-sm-6">
            <div class="form-group">
                <label for="parent_id">Parent</label>
                <select class="form-control {{ $errors->has('parent_id') ? 'is-invalid' : null }}" name="parent_id" id="parent_id">
                    <option value="">(select)</option>
                    @foreach($groups as $item)
                        <option value="{{ $item->getKey() }}" {{ isset($group) && $group->parent_id == $item->getKey() ? 'selected' : null }}>{{ $item->name }}</option>
                    @endforeach
                </select>
                {!! $errors->has('parent_id') ? '<div class="invalid-feedback">' . $errors->first('parent_id') . '</div>' : null !!}
            </div>
        </div>
    </div>
    
    @if(isset($group)) 
        <button type="submit" name="_method" value="put" class="btn btn-primary">Update</button>
    @else
        <button type="submit" class="btn btn-success">Add</button>
    @endif
</form>