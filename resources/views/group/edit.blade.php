@extends('layouts.app')

@section('content')
    <div class="page-header mb-5">
        <h1>Edit group - {{ $group->name }}</h1>
    </div>

    @include('group.forms.group')
@endsection