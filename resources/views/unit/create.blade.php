@extends('layouts.app')

@section('content')
    <div class="page-header mb-5">
        <h1>Create new unit</h1>
    </div>

    @include('unit.forms.unit')
@endsection