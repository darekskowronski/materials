@extends('layouts.app')

@section('content')
    <div class="page-header mb-5">
        <h1>Edit unit - {{ $unit->name }}</h1>
    </div>

    @include('unit.forms.unit')
@endsection