@extends('layouts.app')

@section('content')
    <div class="page-header mb-5">
        <h1>Units</h1>
        <a href="{{ route('units.create') }}" class="btn btn-primary">Add new unit</a>
    </div>

    <ul>
        @foreach($units as $item)
            <li>
                <a href="{{ route('units.edit', ['id' => $item->getKey()]) }}">{{ $item->name }}</a>
            </li>
        @endforeach
    </ul>
@endsection