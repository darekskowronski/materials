<form method="post" action="{{ isset($unit) ? route('units.update', ['id' => $unit->getKey()]) : route('units.store') }}">
    {{ csrf_field() }}
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : null }}" id="name" name="name" value="{{ isset($unit) ? $unit->name : null }}" required>
                {!! $errors->has('name') ? '<div class="invalid-feedback">' . $errors->first('name') . '</div>' : null !!}
            </div>
        </div>
        
        <div class="col-sm-6">
            <div class="form-group">
                <label for="shortcut">Shortcut</label>
                <input type="shortcut" class="form-control {{ $errors->has('shortcut') ? 'is-invalid' : null }}" id="shortcut" name="shortcut" value="{{ isset($unit) ? $unit->shortcut : null }}" required>
                {!! $errors->has('shortcut') ? '<div class="invalid-feedback">' . $errors->first('shortcut') . '</div>' : null !!}
            </div>
        </div>
    </div>
    
    @if(isset($unit)) 
        <button type="submit" name="_method" value="put" class="btn btn-primary">Update</button>
    @else
        <button type="submit" class="btn btn-success">Add</button>
    @endif
</form>
