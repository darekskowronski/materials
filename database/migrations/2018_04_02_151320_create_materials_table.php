<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 3);
            $table->string('name');
            $table->integer('group_id')->unsigned()->index();
            $table->integer('unit_id')->unsigned()->nullable()->index();
            
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('unit_id')->references('id')->on('groups')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
