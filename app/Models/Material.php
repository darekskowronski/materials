<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    public $timestamps = false;
    
    /**
     * Relation to unit
     * 
     * @return \App\Models\Unit
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }
    
    /**
     * Relation to group
     * 
     * @return \App\Models\Group
     */
    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }
    
    /**
     * Save material model
     * 
     * @param array $input
     */
    public function saveMaterial(array $input)
    {
        $this->code = $input['code'];
        $this->name = $input['name'];
        $this->group_id = $input['group_id'];
        $this->unit_id = isset($input['unit_id']) ? $input['unit_id'] : null;
        
        $this->saveOrFail();
    }
}
