<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    public $timestamps = false;
    
    /**
     * Relation to materials
     * 
     * @return \Illuminate\Support\Collection
     */
    public function materials()
    {
        return $this->hasMany(Material::class, 'unit_id');
    }
    
    /**
     * Save unit model
     * 
     * @param array $input
     */
    public function saveUnit(array $input)
    {
        $this->name = $input['name'];
        $this->shortcut = $input['shortcut'];
        
        $this->saveOrFail();
    }
}
