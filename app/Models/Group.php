<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $timestamps = false;
    
    /**
     * Relation to parent group
     * 
     * @return \App\Models\Group
     */
    public function parentGroup()
    {
        return $this->belongsTo(Group::class, 'parent_id');
    }
    
    /**
     * Relation to materials
     * 
     * @return \Illuminate\Support\Collection
     */
    public function materials()
    {
        return $this->hasMany(Material::class, 'group_id');
    }
    
    /**
     * Save group model
     * 
     * @param array $input
     */
    public function saveGroup(array $input)
    {
        $this->name = $input['name'];
        $this->parent_id = isset($input['parent_id']) ? $input['parent_id'] : null;
        
        $this->saveOrFail();
    }
}
