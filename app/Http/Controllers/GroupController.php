<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupRequest as Request;
use App\Models\Group;

class GroupController extends Controller
{
    protected $group;
    
    /**
     * @param \App\Models\Group $group
     */
    public function __construct(Group $group)
    {
        $this->group = $group;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = $this->group->all();
        
        return view('group.index', [
            'groups' => $groups,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = $this->group->all();
        
        return view('group.create', [
            'groups' => $groups,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->group->saveGroup($request->input());
            $message = 'success|Added';
        } catch(\Exception $ex) {
            $message = 'danger|Error';
        }
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = $this->group->findOrFail($id);
        $groups = $this->group->where('id', '!=', $id)->get();
        
        return view('group.edit', [
            'group' => $group,
            'groups' => $groups,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $group = $this->group->findOrFail($id);
            $group->saveGroup($request->input());
            $message = 'success|Updated';
        } catch(\Exception $ex) {
            $message = 'danger|Error';
        }
        
        return redirect()->back()->with('message', $message);
    }
}
