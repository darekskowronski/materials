<?php

namespace App\Http\Controllers;

use App\Http\Requests\MaterialRequest as Request;
use App\Models\Material;
use App\Models\Group;
use App\Models\Unit;

class MaterialController extends Controller
{
    protected $material;
    
    /**
     * @param \App\Models\Material $material
     */
    public function __construct(Material $material)
    {
        $this->material = $material;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = $this->material->all();
        
        return view('material.index', [
            'materials' => $materials,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @param \App\Models\Group $group
     * @param \App\Models\Unit $unit
     * @return \Illuminate\Http\Response
     */
    public function create(Group $group, Unit $unit)
    {
        $groups = $group->all();
        $units = $unit->all();
        
        return view('material.create', [
            'groups' => $groups,
            'units' => $units,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->material->saveMaterial($request->input());
            $message = 'success|Added';
        } catch(\Exception $ex) {
            $message = 'danger|Error';
        }
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @param \App\Models\Group $group
     * @param \App\Models\Unit $unit
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Group $group, Unit $unit)
    {
        $material = $this->material->findOrFail($id);
        
        $groups = $group->all();
        $units = $unit->all();
        
        return view('material.edit', [
            'material' => $material,
            'groups' => $groups,
            'units' => $units,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $material = $this->material->findOrFail($id);
            $material->saveMaterial($request->input());
            $message = 'success|Updated';
        } catch(\Exception $ex) {
            $message = 'danger|Error';
        }
        
        return redirect()->back()->with('message', $message);
    }
}
