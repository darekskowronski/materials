<?php

namespace App\Http\Controllers;

use App\Http\Requests\UnitRequest as Request;
use App\Models\Unit;

class UnitController extends Controller
{
    protected $unit;
    
    /**
     * @param \App\Models\Unit $unit
     */
    public function __construct(Unit $unit)
    {
        $this->unit = $unit;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = $this->unit->all();
        
        return view('unit.index', [
            'units' => $units,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->unit->saveUnit($request->input());
            $message = 'success|Added';
        } catch(\Exception $ex) {
            $message = 'danger|Error';
        }
        
        return redirect()->back()->with('message', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = $this->unit->findOrFail($id);
        
        return view('unit.edit', [
            'unit' => $unit,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $unit = $this->unit->findOrFail($id);
            $unit->saveUnit($request->input());
            $message = 'success|Updated';
        } catch(\Exception $ex) {
            $message = 'danger|Error';
        }
        
        return redirect()->back()->with('message', $message);
    }
}
