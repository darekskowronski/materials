<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('is_not_looped', function($attribute, $value, $parameters, $validator) {
            if(strtolower(request()->method()) == 'post')
                return true;
            
            $groupId = request()->group;
            $parentId = $value;
            
            return !((new \App\Models\Group)->where('id', $parentId)->where('parent_id', $groupId)->first());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
