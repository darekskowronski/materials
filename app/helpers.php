<?php

if(!function_exists('displayFlashMessage')) {
    /**
     * Display flash message
     * 
     * @return string|null
     */
    function displayFlashMessage() {
        $flashMessage = request()->session()->get('message');

        if($flashMessage) {
            list($type, $message) = explode('|', $flashMessage);

            return sprintf('<div class="alert alert-%s">%s</div>', $type, $message);
        }

        return null;
    }
}

