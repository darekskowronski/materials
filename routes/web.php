<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('materials', 'MaterialController', [
    'except' => [
        'show',
        'destroy',
    ],
]);

Route::resource('groups', 'GroupController', [
    'except' => [
        'show',
        'destroy',
    ],
]);

Route::resource('units', 'UnitController', [
    'except' => [
        'show',
        'destroy',
    ],
]);

Route::get('/', function () {
    return redirect(route('materials.index'));
});
