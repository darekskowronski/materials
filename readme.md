## Instalacja zależności
Aby zainstalować wszystkie zależności, należy wydać polecenie:
`composer install`

## Baza danych
Parametry połączenia z bazą danych znajdują się w pliku `.env`
- `DB_HOST` - adres hosta MySQL
- `DB_PORT` - port MySQL
- `DB_DATABASE` - nazwa bazy danych
- `DB_USERNAME` - nazwa użytkownika
- `DB_PASSWORD` - hasło

Aby stworzyć skrutkurę tabel można wydać polecenie:
`php artisan migrate`

Alternatywnie, w głównym katalogu znajduje się plik `materials.sql`, który można zaimportować do bazy danych, tym samym wypełniając ją przykładowymi danymi
